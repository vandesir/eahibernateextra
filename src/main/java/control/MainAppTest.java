package control;

import static org.junit.Assert.*;

import org.junit.Test;

public class MainAppTest {

	@Test
	public void test() {
		MainApp.addData();
		assertTrue("",MainApp.queryResult1.size() == 2);
		assertTrue("",MainApp.queryResult2.size() == 1);
		assertTrue("",MainApp.queryResult3.size() == 1);
		//assertTrue("",MainApp.queryResult4.size() == 1);
		assertTrue("",MainApp.queryResult5.size() == 1);
	}

}
