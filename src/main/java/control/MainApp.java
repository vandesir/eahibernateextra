package control;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import models.Project;
import models.Resource;
import models.Status;
import models.Task;
import models.UserRoles.Administrator;
import models.UserRoles.Beneficiariy;
import models.UserRoles.Volunteer;

public class MainApp {
	private static EntityManagerFactory emf;
	
	public static List<Object[]> queryResult1;
	public static List<Project> queryResult2;
	public static List<Project> queryResult3;
	public static List<Project> queryResult4;
	public static List<Object[]> queryResult5;

	static {
		try {
			emf = Persistence.createEntityManagerFactory("cs544EX");
		} catch (Throwable ex) {
			ex.printStackTrace();
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void main(String[] args) {

		addData();
		emf.close();
	}

	
	public static void addData() {
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			
			Calendar cal = Calendar.getInstance();

			//Add Users
			
			
			Administrator ad2 = new Administrator();
			ad2.setName("Admin2");
			
			//Add Volunteer
			Volunteer v1 = new Volunteer();
			v1.setName("Vol1");
			
			Volunteer v2 = new Volunteer();
			v2.setName("Vol2");
			
			//Add Beneficiary
			Beneficiariy b1 = new Beneficiariy();
			b1.setName("Bene1");
			
			Beneficiariy b2 = new Beneficiariy();
			b2.setName("Bene2");
			
			//create Resource
			Resource re1 = new Resource();
			re1.setName("Res 1");
			
			Resource re2 = new Resource();
			re2.setName("Res 2");
			
			
			//create Task
			Task t1= new Task();
			t1.setName("Task1");
			t1.setVolunteers(Arrays.asList(v1,v2));
			t1.setResources(Arrays.asList(re1));
			t1.setTaskStatus(Status.IN_PROGRESS);
			cal.set(2017, 4, 12, 18, 30,0);
			t1.setEndDate(cal.getTime());
			t1.setStartDate(Calendar.getInstance().getTime());
			
			
			Task t2= new Task();
			t2.setName("Task2");
			t2.setVolunteers(Arrays.asList(v1));
			t2.setResources(Arrays.asList(re1,re2));
			t2.setTaskStatus(Status.COMPLETED);
			cal.set(2017, 1, 11, 5, 0,0);
			t2.setEndDate(cal.getTime());
			t2.setStartDate(Calendar.getInstance().getTime());
			
			//v1.setTasks(Arrays.asList(t1,t2));
			//v2.setTasks(Arrays.asList(t1));
		// TODO your code
			Project prj=new Project();
			prj.setLocation("Fairfield");
			prj.setEndDate(Calendar.getInstance().getTime());
			prj.setName("Project with name one");
			prj.setProjectStatus(Status.COMPLETED);
			prj.setBenificiries(Arrays.asList(b1,b2));
			
			prj.setTasks(Arrays.asList(t1,t2));
			
			
			cal.set(2017, 9, 12, 18, 30,0);
			
			prj.setStartDate(cal.getTime());
			
			
			Administrator ad1 = new Administrator();
			ad1.setName("Admin1");
			ad1.setProjects(Arrays.asList(prj));
			
			
			prj.setProjectAdministrator(ad1);
			t1.setProject(prj);
			t2.setProject(prj);
			
			re1.setTask(t1);
			re1.setTask(t2);
			re2.setTask(t1);
			
			em.persist(ad1);
			
			
			
			queryResult1 = em.createQuery("SELECT p.name,p.location,p.projectStatus,t.name FROM Project p JOIN p.tasks t").getResultList();
			for(Object[] arr:queryResult1){
				for(Object ob:arr){
					System.out.print(","+ob.toString());
				}
				System.out.println("\n");
			}
			
			System.out.println("-----------------------------------");
			
			queryResult2 = em.createQuery("SELECT p FROM Project p ORDER BY p.projectStatus").getResultList();
			for(Project p:queryResult2){
				System.out.println(p.getName()+","+p.getProjectStatus());
				System.out.println("\n");
			}
			
			System.out.println("-----------------------------------");
			queryResult3 = em.createQuery("SELECT p FROM Project p JOIN p.tasks t JOIN t.resources r WHERE r.name='Res 1'").getResultList();
			for(Project p:queryResult3){
				System.out.println(p.getName()+","+p.getProjectStatus());
				System.out.println("\n");
			}
			
			/*System.out.println("-----------------------------------");
			queryResult4 = em.createQuery("SELECT p FROM Project p WHERE p.name LIKE '%1%' AND p.location LIKE '%field%'").getResultList();
			for(Project p:queryResult4){
				System.out.println(p.getName()+"....."+p.getProjectStatus());
				System.out.println("\n");
			}*/
			
			System.out.println("-----------------------------------");
			queryResult5 = em.createQuery("SELECT  p,t FROM Project p JOIN p.tasks t WHERE SIZE(t.volunteers)=1 ORDER BY t.startDate").getResultList();
			for(Object[] arr:queryResult5){
				System.out.println(((Project)arr[0]).getName());
				//+","+((Task)arr[1]).getName()
				System.out.println(arr[1].toString());
				
				System.out.println("\n");
			}

			tx.commit();
		} catch (Throwable e) {
			if ((tx != null) && (tx.isActive())) tx.rollback();
			System.out.println("----------------------------");
			e.printStackTrace();
		} finally {
			if ((em != null) && (em.isOpen())) em.close();
			System.out.println("----------------------------");
			System.out.println("ALL DONE");
		}
	}
}
