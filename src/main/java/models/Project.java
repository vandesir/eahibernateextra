package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import models.UserRoles.Administrator;
import models.UserRoles.Beneficiariy;
import models.UserRoles.User;

@Entity
public class Project {
	@Id @GeneratedValue
	private int projectId;
	private String name;
	
	@Temporal(TemporalType.DATE)
	private Date startDate;
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@Enumerated(EnumType.STRING)
	private Status projectStatus;
	
	private String location;
	
	@ManyToOne
	@JoinColumn(name="administratorId")
	private Administrator projectAdministrator;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinTable(name="ProjectBeneficieries")
	private List<Beneficiariy> benificiries = new ArrayList<Beneficiariy>();	
	
	@OneToMany(mappedBy = "project",cascade={CascadeType.ALL})
	private List<Task> tasks = new ArrayList<Task>();
	
	
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	public Administrator getProjectAdministrator() {
		return projectAdministrator;
	}
	public void setProjectAdministrator(Administrator projectAdministrator) {
		this.projectAdministrator = projectAdministrator;
	}
	public List<Beneficiariy> getBenificiries() {
		return benificiries;
	}
	public void setBenificiries(List<Beneficiariy> benificiries) {
		this.benificiries = benificiries;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Status getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(Status projectStatus) {
		this.projectStatus = projectStatus;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
