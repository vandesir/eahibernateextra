package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Resource {

	@Id @GeneratedValue
	private int recourceId;
	
	@ManyToOne
	@JoinColumn(name="taskId")
	private Task task;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRecourceId() {
		return recourceId;
	}

	public void setRecourceId(int recourceId) {
		this.recourceId = recourceId;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}
}
