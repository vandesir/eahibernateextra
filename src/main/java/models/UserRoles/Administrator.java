package models.UserRoles;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import models.Project;

@Entity
public class Administrator extends User{
	
	@OneToMany(mappedBy="projectAdministrator",cascade={CascadeType.ALL})
	private List<Project> projects = new ArrayList<Project>();

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

}
