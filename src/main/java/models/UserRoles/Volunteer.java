package models.UserRoles;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import models.Task;

@Entity
public class Volunteer extends User{
	
	@ElementCollection
	@ManyToMany(mappedBy = "volunteers")
	private List<Task> tasks = new ArrayList<Task>();

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

}
